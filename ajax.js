$(document).ready(function () {

    var galleryRegexp = /(.+)index\.php\?id=(.+)$/;
    $image = $("#loading-image");

    $("ul#nav a").click(function () {
        $image.css("display", "block");

        $('li').removeClass('current');
        $(this).parent(this).addClass('current');

        var url = $(this).attr("href");
        $("#content").load(url + " #content > *");

        history.pushState('', '', url);
        event.preventDefault();
        return false;
    });



    $(document).ajaxStart(function () {
        $image.fadeIn("slow", 1);

        $image.animate(function () {
            $image.css("display", "block");
        });


    }).ajaxComplete(function () {

        $image.fadeTo("slow", 0);

        $image.animate(function () {
            $image.css("display", "none");
        });


        if (location.href.match(galleryRegexp)) {
            location.href += '&';
        } else {
            location.href += ' ';
        }


    });
});
